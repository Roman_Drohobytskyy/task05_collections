package com.drohobytskyy.collections.Controller;

import com.drohobytskyy.collections.Model.BinaryTree;
import com.drohobytskyy.collections.View.ConsoleMapView;

import java.io.IOException;

public class Controller {
    private BinaryTree tree;

    public Controller(ConsoleMapView consoleMapView) throws IOException {
        this.tree = new BinaryTree();
        tree.view = consoleMapView;
        tree.execute();
        consoleMapView.controller = this;
        consoleMapView.initMenu();
        consoleMapView.show();
    }

    public void showTree() {
        tree.inOrder(tree.root);
    }

    public void findMinimum() {
        tree.showNodeWithMinimumKey();
    }

    public void findMaximum() {
        tree.showNodeWithMaximumKey();
    }

    public void insertEmployee() {
        tree.insertByKey("Volodia", "ZE");
    }

    public void removeEmployee() {
        tree.removeByKey(1234);
    }
}
