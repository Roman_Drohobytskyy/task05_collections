package com.drohobytskyy.collections.Model;

public class Node {
    int id;
    Employee employee;
    Node leftChild;
    Node rightChild;

    public Node(Employee employee) {
        this.id = employee.getId();
        this.employee = employee;
    }

    @Override
    public String toString() {
        return "Node{" +
                "id=" + id +
                ", employee=" + employee +
                ", leftChild=" + leftChild +
                ", rightChild=" + rightChild +
                '}';
    }
}
