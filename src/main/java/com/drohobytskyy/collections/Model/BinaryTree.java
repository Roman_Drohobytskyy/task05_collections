package com.drohobytskyy.collections.Model;

import com.drohobytskyy.collections.View.ConsoleMapView;

public class BinaryTree {
    private int length = 0;
    public ConsoleMapView view;
    public Node root;

    public void execute() {
        insert(new Employee("Roman", "Drohobytskyy"));
        insert(new Employee("Andrew", "Petrov"));
        insert(new Employee("Marjan", "Ivanov"));
        insert(new Employee("Vova", "putin"));
    }

    private Node find(int key) {
        if (length == 0) {
            return null;
        }
        Node current = root;
        while (current.id != key) {
            if (key < current.id) {
                current = current.leftChild;
            } else {
                current = current.rightChild;
            }
            if (current == null) {
                return null;
            }
        }
        return current;
    }

    private void insert(Employee employee) {
        Node newNode = new Node(employee);
        if (root == null) {
            root = newNode;
        } else {// If root node exists
            Node current = root;
            Node parent;
            while (true) {
                parent = current;
                if (employee.getId() < current.id) {// Go left?
                    current = current.leftChild;
                    if (current == null) {// If a leaf is founded - insert to the left
                        parent.leftChild = newNode;
                        return;
                    }
                } else { // Go right?
                    current = current.rightChild;
                    if (current == null) {// If we are in the end of a tree,
                        parent.rightChild = newNode; // insert to the right
                        return;
                    }
                }
            }
        }
        length++;
    }

    private boolean delete(int key) {
        if (length == 0) {
            return false;
        }
        Node current = root;
        Node parent = root;
        boolean isLeftChild = true;
        while (current.id != key) {
            parent = current;
            if (key < current.id) {// Go left?
                isLeftChild = true;
                current = current.leftChild;
            } else { // Go right?
                isLeftChild = false;
                current = current.rightChild;
            }
            if (current == null) {
                return false; // Node doesn't exist!
            }
        }
        // If Node to delete doesn't have children (it's a leaf or the root):
        if (current.leftChild == null && current.rightChild == null) {
            if (current == root) {
                root = null;
            } else if (isLeftChild) {
                parent.leftChild = null;
            } else {
                parent.rightChild = null;
            }
        }
        // If Node to delete has only one child:
        else if (current.rightChild == null) {
            if (current == root) {
                root = current.leftChild; // If it's the root node then left child'll become the root node.
            } else if (isLeftChild) {// Left child
                parent.leftChild = current.leftChild;
            } else {// Right child
                parent.rightChild = current.leftChild;
            }
        // If left child doesn't exist, node'll be replaced by the right one.
        } else if (current.leftChild == null) {
            if (current == root) {
                root = current.rightChild;
            } else if (isLeftChild) {// Left child
                parent.leftChild = current.rightChild;
            } else {// Right child
                parent.rightChild = current.rightChild;
            }
        } else {// Two children, node'll be replaced by a successor.
            Node successor = getSuccessor(current); // Get successor.
            if (current == root) {
                root = successor;
            } else if (isLeftChild) {
                parent.leftChild = successor;
            } else {
                parent.rightChild = successor;
            }
            successor.leftChild = current.leftChild;
        } // Finish for two children
        length--;
        return true;
    }

    private Node getSuccessor(Node delNode) {
        Node successorParent = delNode;
        Node successor = delNode;
        Node current = delNode.rightChild;
        while (current != null) {
            successorParent = successor;
            successor = current;
            current = current.leftChild;
        }
        // If a successor isn't right child, create create links between nodes.
        if (successor != delNode.rightChild) {
            successorParent.leftChild = successor.rightChild;
            successor.rightChild = delNode.rightChild;
        }
        return successor;
    }

    public void inOrder(Node localRoot) {
        if (localRoot != null) {
            inOrder(localRoot.leftChild);
            view.logger.info(localRoot.employee);
            inOrder(localRoot.rightChild);
        }
    }

    private Node returnNodeWithMinimumKey() {
        Node current = root;
        Node last = null;
        while (current != null) {
            last = current;
            current = current.leftChild;
        }
        return last;
    }

    private Node returnNodeWithMaximumKey() {
        Node current = root;
        Node last = null;
        while (current != null) {
            last = current;
            current = current.rightChild;
        }
        return last;
    }

    public void findByKey (int keyToFound) {
        inOrder(root);
        Node found = find(keyToFound);
        if (found != null) {
            view.logger.info("Found the node with key: " + keyToFound);
        } else {
            view.logger.info("Could not find node with key: " + keyToFound);
        }
    }

    public void showNodeWithMinimumKey() {
        view.logger.info("node with minimum key: " + returnNodeWithMinimumKey());
    }

    public void showNodeWithMaximumKey() {
        view.logger.info("node with MAXimum key: " + returnNodeWithMaximumKey());
    }

    public void removeByKey(int key) {
        inOrder(root);
        delete(key);
        view.logger.info("After removing: ");
        inOrder(root);
    }

    public void insertByKey(String name, String surname) {
        insert(new Employee(name, surname));
        inOrder(root);
    }

}

