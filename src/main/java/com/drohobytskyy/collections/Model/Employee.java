package com.drohobytskyy.collections.Model;

import java.util.HashSet;
import java.util.Objects;
import java.util.Random;
import java.util.Set;

public class Employee {
    private static final int ID_MAX_VALUE = 1000;
    private static Set<Integer> uniqueIdSet = new HashSet();
    private Random random = new Random();
    private int id;
    private String name;
    private String surname;

    public Employee(String name, String surname) {
        this.id = generateUniqueId ();
        this.setName(name);
        this.setSurname(surname);
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name != "" && name != null) {
            this.name = name;
        }
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        if (surname != "" && surname != null) {
            this.surname = surname;
        }
    }

    private int generateUniqueId () {
        int result;
        do {
            result = random.nextInt(ID_MAX_VALUE);
        } while (uniqueIdSet.contains(result));
        uniqueIdSet.add(result);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return id == employee.id &&
                name.equals(employee.name) &&
                surname.equals(employee.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, surname);
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                '}';
    }
}
