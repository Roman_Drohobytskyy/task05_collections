package com.drohobytskyy.collections;

import com.drohobytskyy.collections.Controller.Controller;
import com.drohobytskyy.collections.Model.BinaryTree;
import com.drohobytskyy.collections.View.ConsoleMapView;
import java.io.IOException;

public class StartPoint {
    public static void main(String[] args) {
        try {
            Controller controller = new Controller(new ConsoleMapView());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
