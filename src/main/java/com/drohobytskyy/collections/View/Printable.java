package com.drohobytskyy.collections.View;

@FunctionalInterface
public interface Printable {

    void print();
}

