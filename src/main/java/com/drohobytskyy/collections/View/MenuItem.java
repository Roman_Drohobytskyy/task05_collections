package com.drohobytskyy.collections.View;

public class MenuItem {
    private String description;
    private Printable link;

    public MenuItem(String description, Printable link) {
        this.description = description;
        this.link = link;
    }

    public String getDescription() {
        return description;
    }

    public Printable getLink() {
        return link;
    }
}
