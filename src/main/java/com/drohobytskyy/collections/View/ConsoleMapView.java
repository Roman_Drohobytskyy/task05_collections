package com.drohobytskyy.collections.View;

import com.drohobytskyy.collections.Controller.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ConsoleMapView {
    private static String EXIT = "exit";
    public Controller controller;
    private Map<String, MenuItem> menu;
    private BufferedReader input;
    public Logger logger;

    public ConsoleMapView() {
        input = new BufferedReader(new InputStreamReader(System.in));
        logger = LogManager.getLogger(ConsoleMapView.class);
    }

    public void initMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", new MenuItem("  1      - Show a tree",
                controller::showTree));
        menu.put("2", new MenuItem("  2      - Find a node with a minimum key value",
                controller::findMinimum));
        menu.put("3", new MenuItem("  3      - Find a node with a MAXimum key value",
                controller::findMaximum));
        menu.put("4", new MenuItem("  4      - Insert new employee into the tree",
                controller::insertEmployee));
        menu.put("5", new MenuItem("  5      - Remove employee from the tree by ID number",
                controller::removeEmployee));
        menu.put(EXIT, new MenuItem(EXIT + "   - exit", this::exitFromMenu));
    }

    public void show() throws IOException {
        String keyMenu;
        do {
            logger.warn("Please, select menu point.");
            outputMenu();
            keyMenu = input.readLine().toLowerCase();
            try {
                menu.get(keyMenu).getLink().print();
            } catch (Exception e) {
                logger.error("Invalid input! Please try again.");
            }
        } while (true);
    }

    private void outputMenu() {
        for (MenuItem item : menu.values()) {
            logger.info(item.getDescription());
        }
    }

    private void exitFromMenu() {
        logger.info("Have an amazing day!");
        System.exit(0);
    }
}